This is an example setup of laravel using vue as a frontend

1) create new database
2) create .env file and modify
3) connect database in .env file
4) run composer install
5) run php artisan key:generate
6) run npm install
7) run php artisan migrate
8) run php artisan db:seed
9) delete all files from storage folder and manually create the following structure

  storage
    app
    framework
      cache
        data
      sessions
      testing
      views
    logs

10) run composer dump-autoload ?
11) run php artisan config:cache


for development with vue, run the run npm watch script
