<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
  <a href="#" class="navbar-brand">Brand</a>
  <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarMenu">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarMenu">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item"><a class="nav-link" href="{{ route('admin') }}">Home</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ route('contents.list') }}">Contents</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ route('cases.list') }}">Cases</a></li>
      @if(Auth::check())
        <li class="nav-item"><a class="nav-link" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
      </li>
      @endif
    </ul>
  </div>
</nav>
