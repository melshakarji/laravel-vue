<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false, 'reset' => false]);
Route::middleware(['auth'])->group(function () {
  Route::get('/admin', 'AdminController@index')->name('admin');
  Route::get('/admin/contents', 'ContentController@index')->name('contents.list');
  Route::get('/admin/cases', 'ProjectController@index')->name('cases.list');
  Route::get('/admin/cases/create', 'ProjectController@create')->name('cases.create');
});
Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/cases', 'ProjectController@cases')->name('cases');
Route::get('/contact', 'HomeController@contact')->name('contact');
