<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Requests;
use App\Http\Resources\Project as ProjectResource;

class ProjectController extends Controller
{
    public function cases()
    {
      return view('cases.index');
    }

    public function index()
    {
      return view('admin.cases.index');
    }

    public function list()
    {
      $cases = Project::orderBy('created_at', 'desc')->paginate(15);
      return ProjectResource::collection($cases);
    }
}
